# openMV-TensorFlowLite

#### Description
使用在线训练模型网站Edge Impulse对TensorFlowLite神经网络进行训练，最终部署到open MV 4 Plus上运行，进行垃圾识别的训练好的模型，可直接使用。
内部包含：防静电袋、纸箱、电子器件、电子废弃物、电子线、泡沫纸、高温海绵、纸类、焊料、工具，10种分类，训练验证的识别准确率97%。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
